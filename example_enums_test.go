package listenums

type AA int8

const (
	AAUnknown AA = iota
	AAOne
	AATwo
)

type Fl float64

const (
	FloatOne Fl = iota + 1
	FloatTwo
)

type Digit byte

const (
	DigitOne Digit = iota + 3
	DigitTwo
	DigitThree
	DigitFour
)

type Status string

const (
	StatusOne   Status = "one"
	StatusTwo   Status = "two"
	StatusThree Status = "three"
)

const (
	StatusFour Status = "four"
)

const StatusFive Status = "five"
