package listenums

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_constants(t *testing.T) {
	cases := map[string]struct {
		fileName       string
		typeName       string
		expected       map[string]interface{}
		expectedErrStr string
	}{

		"Aaa": {
			fileName: "example_enums_test.go",
			typeName: "AA",
			expected: map[string]interface{}{
				"AAUnknown": int64(0),
				"AAOne":     int64(1),
				"AATwo":     int64(2),
			},
		},
		"Float constant": {
			fileName: "example_enums_test.go",
			typeName: "Fl",
			expected: map[string]interface{}{
				"FloatOne": int64(1),
				"FloatTwo": int64(2),
			},
		},

		"Digit constant": {
			fileName: "example_enums_test.go",
			typeName: "Digit",
			expected: map[string]interface{}{
				"DigitOne":   int64(3),
				"DigitTwo":   int64(4),
				"DigitThree": int64(5),
				"DigitFour":  int64(6),
			},
		},
		"Status constant": {
			fileName: "example_enums_test.go",
			typeName: "Status",
			expected: map[string]interface{}{
				"StatusOne":   "one",
				"StatusTwo":   "two",
				"StatusThree": "three",
				"StatusFour":  "four",
				"StatusFive":  "five",
			},
		},
	}
	for name, c := range cases {
		t.Run(name, func(t *testing.T) {
			actual, err := constants(c.fileName, c.typeName)
			if c.expectedErrStr != "" {
				assert.EqualError(t, err, c.expectedErrStr)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, c.expected, actual)
		})
	}
}
