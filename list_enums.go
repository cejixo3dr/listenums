package listenums

import (
	"go/ast"
	"go/parser"
	"go/token"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

func Keys[T int64 | string](m map[string]T, excludes ...string) []string {
	excludeIdx := make(map[string]struct{})
	for _, excl := range excludes {
		excludeIdx[excl] = struct{}{}
	}

	res := make([]string, 0, len(m))
	for key := range m {
		_, ok := excludeIdx[key]
		if ok {
			continue
		}

		res = append(res, key)
	}

	return res
}

func Values[T int64 | string](m map[string]T, excludes ...T) []T {
	excludeIdx := make(map[T]struct{})
	for _, excl := range excludes {
		excludeIdx[excl] = struct{}{}
	}

	res := make([]T, 0, len(m))
	for _, val := range m {
		_, ok := excludeIdx[val]
		if ok {
			continue
		}

		res = append(res, val)
	}

	return res
}

func NumericValues(fileName, typeName string, excludes ...int64) ([]int64, error) {
	m, err := Numeric(fileName, typeName)
	if err != nil {
		return nil, err
	}

	return Values(m, excludes...), nil
}

func StringValues(fileName, typeName string, excludes ...string) ([]string, error) {
	m, err := String(fileName, typeName)
	if err != nil {
		return nil, err
	}

	return Values(m, excludes...), nil
}

func Numeric(fileName, typeName string) (map[string]int64, error) {
	m, err := constants(fileName, typeName)
	if err != nil {
		return nil, err
	}

	res := make(map[string]int64, len(m))
	for name, val := range m {
		i, ok := val.(int64)
		if !ok {
			return nil, errors.Errorf("constant with the name (%s) is not numeric and has value (%v)", name, val)
		}
		res[name] = i
	}

	return res, nil
}

func String(fileName, typeName string) (map[string]string, error) {
	m, err := constants(fileName, typeName)
	if err != nil {
		return nil, err
	}

	res := make(map[string]string, len(m))
	for name, val := range m {
		s, ok := val.(string)
		if !ok {
			return nil, errors.Errorf("constant with the name (%s) is not string and has value (%v)", name, val)
		}
		res[name] = s
	}

	return res, nil
}

// constants - will return list of constants that associated with the given type name and also provide theirs values.
// I don't know how this code works, but it works :).
func constants(fileName, typeName string) (map[string]interface{}, error) {
	tokenSet := token.NewFileSet()
	node, err := parser.ParseFile(tokenSet, fileName, nil, parser.ParseComments)
	if err != nil {
		return nil, errors.Wrapf(err, "can't parse file %s", fileName)
	}

	typeObject, err := findType(node.Scope, typeName)
	if err != nil {
		return nil, errors.Wrapf(err, "file %s does not contain type", fileName)
	}

	result := make(map[string]interface{})
	for _, declaration := range node.Decls {
		genDecl, ok := declaration.(*ast.GenDecl)
		if !ok {
			continue
		}

		if genDecl.Tok != token.CONST {
			continue
		}

		var (
			isIota         bool
			iotaFirstValue int64
		)
		for _, spec := range genDecl.Specs {
			vSpec, ok := spec.(*ast.ValueSpec)
			if !ok {
				continue
			}

			if isIota {
				iotaFirstValue++
				result[name(vSpec)] = iotaFirstValue

				continue
			}

			it, itfv := iotaf(vSpec, typeName)
			if it {
				isIota = it
				iotaFirstValue = itfv
				result[name(vSpec)] = iotaFirstValue
			} else {
				vType, ok := vSpec.Type.(*ast.Ident)
				if !ok {
					continue
				}
				if vType.Name != typeObject.Name {
					continue
				}

				result[name(vSpec)] = value(vSpec)

			}

		}
	}

	return result, nil
}

func value(spec *ast.ValueSpec) interface{} {
	for _, val := range spec.Values {
		bl, ok := val.(*ast.BasicLit)
		if !ok {
			continue
		}

		val := parseBLit(bl)
		if val == nil {
			continue
		}

		return val
	}
	return nil
}

func name(spec *ast.ValueSpec) string {
	for _, n := range spec.Names {
		return n.Name
	}

	return ""
}

func iotaf(spec *ast.ValueSpec, typeName string) (isIota bool, iotaFirstValue int64) {
	for _, val := range spec.Values {
		binExp, ok := val.(*ast.BinaryExpr)
		if !ok {

			ident, ok := val.(*ast.Ident)
			if ok && ident.Name == "iota" {
				isIota = true
				iotaFirstValue = 0

				break
			}

			continue
		}

		if xIdent, ok := binExp.X.(*ast.Ident); ok && xIdent.Name == "iota" {
			isIota = true
			if bLit, ok := binExp.Y.(*ast.BasicLit); ok {
				val, ok := parseBLit(bLit).(int64)
				if !ok {
					continue
				}
				iotaFirstValue = val
			}
		}
	}
	if isIota {
		if ident, ok := spec.Type.(*ast.Ident); ok {
			if ident.Name == typeName {
				return isIota, iotaFirstValue
			}
		}
	}

	return false, 0
}

func parseBLit(basicLit *ast.BasicLit) (val interface{}) {
	switch basicLit.Kind {
	case token.INT:
		i, err := strconv.ParseInt(basicLit.Value, 10, 64)
		if err != nil {
			return nil
		}

		return i
	case token.STRING:
		return strings.TrimSuffix(strings.TrimPrefix(basicLit.Value, "\""), "\"")
	}

	return nil
}

func findType(s *ast.Scope, name string) (*ast.Object, error) {
	for _, object := range s.Objects {
		if object.Kind == ast.Typ && object.Name == name {
			return object, nil
		}
	}

	return nil, errors.Errorf("can't find type with the name (%s)", name)
}
